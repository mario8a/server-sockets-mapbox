//Configuracion de cada una de las acciones
import { Socket } from 'socket.io';
import socketIO from 'socket.io';
import { UsuariosLista } from '../classes/usuarios-lista';
import { Usuario } from '../classes/usuario';
import { Mapa } from '../classes/mapa';
import { Marcador } from '../classes/marcador';

export const usuariosConectados = new UsuariosLista();
export const mapa = new Mapa();


// EVENTOS DE MAPA
export const mapaSockets = (cliente: Socket, io: socketIO.Server) => {

   cliente.on('marcador-nuevo', (marcador: Marcador) => {
      mapa.agregarMarcador(marcador);
      //emitir a todo el mundo que se creo un nuevo marker menos al cliente porque el lo creo
      // broadcast emite a todos menos al cliente
      cliente.broadcast.emit('marcador-nuevo', marcador);
   });

   cliente.on('marcador-borrar', (id: string) => {
      mapa.borrarMarcador(id);
      cliente.broadcast.emit('marcador-borrar', id);
   });

   //emitir evento y broadcast de marcador-mover
   cliente.on('marcador-mover', (marcador: Marcador) => {
      mapa.moverMarcador(marcador);
      cliente.broadcast.emit('marcador-mover', marcador);
   });

}