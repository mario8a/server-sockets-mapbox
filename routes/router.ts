import { Router, Request, Response } from 'express';
import Server from '../classes/server';
import { Socket } from 'socket.io';
import { usuariosConectados, mapa } from '../sockets/socket';
import { GraficaData } from '../classes/grafica';


const router = Router();




router.get('/mapa', (req: Request, res: Response) => {
   
   res.json(mapa.getMarcadores())
   
});


///  Esto es de otras clases
const grafica = new GraficaData();

router.get('/grafica', (req: Request, res: Response) => {

   res.json(grafica.getDataGrafica())

});

router.post('/grafica', (req: Request, res: Response) => {

   const opcion = Number(req.body.opcion);
   const unidades = Number(req.body.unidades);

   grafica.incrementarValor(opcion, unidades);

   const server = Server.instance;
   server.io.emit('cambio-grafica');

   res.json(grafica.getDataGrafica());

});

router.post('/mensajes/:id', (req: Request, res: Response) => {

   const cuerpo = req.body.cuerpo;
   const de = req.body.de;
   const id = req.params.id;

   const payload = {
      de,
      cuerpo
   }

   // Conectando servicio rest con el servidor de sockets

   const server = Server.instance;
   // in sirve para emitir un mensaje a un usuairo en particular
   server.io.in(id).emit('mensaje-privado', payload);

   res.json({
      ok: true,
      mensaje: 'POST - Listo',
      cuerpo,
      de,
      id
   })

});

// Servicio para obtener todos los ids de los usuarios

router.get('/usuarios', (req: Request, res: Response) => {

   // Conectando servicio rest con el servidor de sockets

   const server = Server.instance;
   // in sirve para emitir un mensaje a un usuairo en particular
   server.io.clients((err: any, clientes: string[]) => {

      if(err) {
         return res.json({
            ok: false,
            err
         })
      }

      res.json({
         ok: true,
         clientes
      });

   });

   
});

// Obtener usuarios y sus nombres

router.get('/usuarios/detalle', (req: Request, res: Response) => {
   
   


   res.json({
      ok: true,
      clientes: usuariosConectados.getLista()
   });

});

export default router;